webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "../../../../../src/app/add-product/add-product.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/add-product/add-product.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n\n  <form (ngSubmit)=\"onSubmit()\">\n    <div>\n      <label for=\"formTitle\">Title</label>\n      <input type=\"text\" [(ngModel)]=\"newProd.title\" name=\"title\" id=\"formTitle\" required>\n    </div>\n\n    <div>\n      <label for=\"formCategory\">Select Category</label>\n      <select [(ngModel)]=\"newProd.category\" name = \"category\" id=\"formCategory\" >\n\n        <option value=\"High\">High Priority</option>\n        <option value=\"Medium\">Medium Priority</option>\n        <option value=\"Low\">Low Prioirty</option>\n\n      </select>\n    </div>\n\n    <div>\n      <label for=\"formDescr\">description</label>\n      <input type=\"text\" [(ngModel)]=\"newProd.description\" name=\"description\" id=\"formDescr\" required>\n    </div>\n\n    <div>\n      <label for=\"formPrice\">price</label>\n      <input type=\"text\" [(ngModel)]=\"newProd.price\" name=\"description\" id=\"formPrice\" required>\n    </div>\n\n    <button type=\"submit\">Submit</button>\n\n  </form>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/add-product/add-product.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var product_service_1 = __webpack_require__("../../../../../src/app/services/product.service.ts");
var AddProductComponent = (function () {
    function AddProductComponent(prodService) {
        this.prodService = prodService;
        this.addProd = new core_1.EventEmitter();
    }
    AddProductComponent.prototype.ngOnInit = function () {
        this.newProd = {
            title: '',
            description: '',
            price: '',
            category: '',
            _id: ''
        };
    };
    AddProductComponent.prototype.onSubmit = function () {
        var _this = this;
        this.prodService.addProd(this.newProd).subscribe(function (res) {
            if (res.success === true) {
                _this.addProd.emit(_this.newProd);
            }
        });
    };
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], AddProductComponent.prototype, "addProd", void 0);
    AddProductComponent = __decorate([
        core_1.Component({
            selector: 'app-add-product',
            template: __webpack_require__("../../../../../src/app/add-product/add-product.component.html"),
            styles: [__webpack_require__("../../../../../src/app/add-product/add-product.component.css")]
        }),
        __metadata("design:paramtypes", [product_service_1.ProductService])
    ], AddProductComponent);
    return AddProductComponent;
}());
exports.AddProductComponent = AddProductComponent;


/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "\n<div style=\"text-align:center\">\n  <h1>\n    {{ title }}\n  </h1>\n\n  <app-view-product (addProd)=\"onAddProd($event)\"></app-view-product>\n\n</div>\n\n\n\n"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var AppComponent = (function () {
    function AppComponent() {
        this.title = 'Pasha APP';
    }
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/app.component.html"),
            styles: [__webpack_require__("../../../../../src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;


/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_1 = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var http_1 = __webpack_require__("../../../common/esm5/http.js");
var forms_1 = __webpack_require__("../../../forms/esm5/forms.js");
var app_component_1 = __webpack_require__("../../../../../src/app/app.component.ts");
var add_product_component_1 = __webpack_require__("../../../../../src/app/add-product/add-product.component.ts");
var view_product_component_1 = __webpack_require__("../../../../../src/app/view-product/view-product.component.ts");
var product_service_1 = __webpack_require__("../../../../../src/app/services/product.service.ts");
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.AppComponent,
                add_product_component_1.AddProductComponent,
                view_product_component_1.ViewProductComponent,
            ],
            imports: [
                platform_browser_1.BrowserModule,
                http_1.HttpClientModule,
                forms_1.FormsModule
            ],
            providers: [product_service_1.ProductService],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;


/***/ }),

/***/ "../../../../../src/app/services/product.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var http_1 = __webpack_require__("../../../common/esm5/http.js");
__webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
var ProductService = (function () {
    function ProductService(http) {
        this.http = http;
        this.serverApi = 'http://localhost:3000';
        this.test = 'test';
    }
    ;
    ProductService.prototype.getAllProds = function () {
        var URI = this.serverApi + "/mainPage";
        return this.http.get(URI)
            .map(function (res) { return res.message; });
    };
    ProductService.prototype.deleteProd = function (prodId) {
        var URI = this.serverApi + "/mainPage/" + prodId;
        return this.http.delete(URI, { headers: { 'Content-Type': 'application/json' } });
    };
    ProductService.prototype.addProd = function (product) {
        var URI = this.serverApi + "/mainPage";
        var body = JSON.stringify({
            title: product.title,
            description: product.description,
            category: product.category,
            price: product.price
        });
        return this.http.post(URI, body, { headers: { 'Content-Type': 'application/json' } })
            .map(function (res) { return res; });
    };
    ProductService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], ProductService);
    return ProductService;
}());
exports.ProductService = ProductService;


/***/ }),

/***/ "../../../../../src/app/view-product/view-product.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/view-product/view-product.component.html":
/***/ (function(module, exports) {

module.exports = "<h2> My Products </h2>\n\n<table id=\"table\">\n  <thead>\n    <tr>\n      <th>Category</th>\n      <th>Title</th>\n      <th>Description</th>\n      <th>Price</th>\n      <th>Delete</th>\n    </tr>\n  </thead>\n  <tbody>\n    <tr *ngFor=\"let product of products\">\n      <td>{{product.category}}</td>\n      <td>{{product.title}}</td>\n      <td>{{product.description}}</td>\n      <td>{{product.price}}</td>\n      <td><button type=\"button\" (click)=\"deleteProd(product); $event.stopPropagation();\">Delete</button></td>\n    </tr>\n  </tbody>\n</table>\n\n<app-add-product (addProd)=\"onAddProd($event)\"></app-add-product>\n"

/***/ }),

/***/ "../../../../../src/app/view-product/view-product.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var product_service_1 = __webpack_require__("../../../../../src/app/services/product.service.ts");
var ViewProductComponent = (function () {
    function ViewProductComponent(productServ) {
        this.productServ = productServ;
        this.products = [];
    }
    ViewProductComponent.prototype.ngOnInit = function () {
        this.loadProds();
    };
    ;
    ViewProductComponent.prototype.loadProds = function () {
        var _this = this;
        this.productServ.getAllProds().subscribe(function (response) {
            _this.products = response;
        });
    };
    ;
    ViewProductComponent.prototype.deleteProd = function (product) {
        var _this = this;
        this.productServ.deleteProd(product._id).subscribe(function (response) { return _this.products = _this.products.filter(function (products) { return products !== product; }); });
    };
    ViewProductComponent.prototype.onAddProd = function (newProd) {
        this.products = this.products.concat(newProd); //why concat?
    };
    ViewProductComponent = __decorate([
        core_1.Component({
            selector: 'app-view-product',
            template: __webpack_require__("../../../../../src/app/view-product/view-product.component.html"),
            styles: [__webpack_require__("../../../../../src/app/view-product/view-product.component.css")]
        }),
        __metadata("design:paramtypes", [product_service_1.ProductService])
    ], ViewProductComponent);
    return ViewProductComponent;
}());
exports.ViewProductComponent = ViewProductComponent;


/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
Object.defineProperty(exports, "__esModule", { value: true });
exports.environment = {
    production: false
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var platform_browser_dynamic_1 = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
var app_module_1 = __webpack_require__("../../../../../src/app/app.module.ts");
var environment_1 = __webpack_require__("../../../../../src/environments/environment.ts");
if (environment_1.environment.production) {
    core_1.enableProdMode();
}
platform_browser_dynamic_1.platformBrowserDynamic().bootstrapModule(app_module_1.AppModule)
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map