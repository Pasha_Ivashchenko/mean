
const express = require('express');
const router = express.Router();
const Product = require('../models/product');

router.get('/',(req,res) => {
    Product.getAllProds((err, products) => {
        if(err) {
            res.json({success: false, message: `CANT LOAD PRODUCTS, ERROR: ${err}`});
        } else {
            res.write(JSON.stringify({success:true, message: products}, null, 2));
            res.end();
        }
    })
});

router.post('/', (req,res,next) => {
    let newProd = new Product({
        title: req.body.title,
        description: req.body.description,
        price: req.body.price,
        category: req.body.category
    });
    Product.addProd(newProd,(err, prod) => {
        if(err) {
            res.json({success: false, message: `CANT CREATE PRODUCT, ERROR: ${err}`});
        } else {
            res.json({success: true, message: "ADDED SUCCESSFULLY"});
        }
    });
});

router.delete('/:id', (req,res,next)=> {
    let id = req.params.id;
    console.log(id);
    Product.deleteProd(id,(err,prod) => {
        if(err) {
            res.json({success:false, message: `FAILED DELETE PRODUCT, ERROR: ${err}`});
        } else if(prod) {
            res.json({success:true, message: "DELETED SUCCESSFULLY"});
        } else {
            res.json({success:false});
        }
    })
});

module.exports = router;