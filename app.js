
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const config = require('./config/database');
const logger = require('./logger');
const app = express();
const port = config.port;
const favicon = require('serve-favicon');

mongoose.Promise = global.Promise;
mongoose.connect(config.database, { useMongoClient: true });

//раскоменить после того как положить favicon в /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

app.use(cors());

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, 'public')));

const mainPage = require('./controllers/mainPage');
app.use('/mainPage', mainPage);

app.listen(port, () => {
    console.log(`MEAN STARTED AT ${port}`);
});



