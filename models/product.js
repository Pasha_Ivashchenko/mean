const mongoose = require('mongoose');

const ProductSchema = mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    description: String,
    price: {
        type: Number,
        required: true
    },
    category: {
        type: String,
        required: true,
        enum: ['High', 'Medium', 'Low']
    }
});

const Product = mongoose.model('Product', ProductSchema);

module.exports = Product;

module.exports.getAllProds = (callback) => {
    Product.find(callback);
};

module.exports.addProd = (newProd, callback) => {
    newProd.save(callback);
};

module.exports.deleteProd = (id, callback) => {
    let query = {_id: id};
    Product.findOneAndRemove(query, callback);
};
