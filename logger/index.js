
const winston = require('winston');
const fs = require('fs');
const env = process.env.NODE_ENV || 'development';
const logDir = 'logger';
const tsFormat = () => (new Date()).toLocaleTimeString();

if (!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir);
};

const logger = new winston.Logger({
    transports: [
        new (winston.transports.Console)({
            timestamp: tsFormat,
            datePattern: 'yyyy-MM-dd',
            colorize: true,
            level: 'info'
        }),
        new (winston.transports.File)({
            filename: `${logDir}/logs.log`,
            timestamp: tsFormat,
            datePattern: 'yyyy-MM-dd',
            level: env === 'development' ? 'error' : 'info'
        })
    ]
});

module.exports = logger;